# 0x0 instance list

Unofficial instance list for 0x0, the null-pointer

| Instance | Country | Provider | File Storage | Max File-Size | Retention | Operator |
|----------|---------|----------|--------------|---------------|-----------|----------|
| https://0x0.st | 🇩🇪 | Hetzner | Hetzner |512 MiB | 197.5 days for 128 MiB | 0x0 developer |
| https://envs.sh | 🇩🇪 | myloc.de | myloc.de | 512 MiB | 197.5 days for 128 MiB | [envs.net](https://envs.net) |
| https://0.tildevarsh.in | 🇮🇳 | Linode | Local Server | 1024 MiB | Forever | [~Tildevarsh](https://tildevarsh.in) |
| https://ttm.sh | 🇨🇦 | OVH | OVH | 256 MiB | 197.5 days for 64 MiB | [~team](https://tilde.team)
| https://0.vern.cc | 🇨🇦 | OVH | Local Server | 1024 MiB | Forever | [~vern](https://vern.cc) |
| [0.vernccvbvyi5...onion](http://0.vernccvbvyi5qhfzyqengccj7lkove6bjot2xhh5kajhwvidqafczrad.onion) | N/A | OVH | Local Server | 1024 MiB | Forever | [~vern](https://vernccvbvyi5qhfzyqengccj7lkove6bjot2xhh5kajhwvidqafczrad.onion) |
| [http://0.vern.i2p](http://vernkjqjz3qctifc3ovi7s77zzkej6qb6wbgly7yu46tgtffskla.b32.i2p) | N/A | OVH | Local Server | 1024 MiB | Forever | [~vern](http://verncceu2kgz54wi7r5jatgmx2mqtsh3knxhiy4m5shescuqtqfa.b32.i2p) |
| https://null.slipfox.xyz | 🇺🇸 | Hetzner | Hetzner | 256 MiB | 197.5 days for 128MiB | [Slipfox Network Suite](https://slipfox.xyz) |

Note: This repo is mirrored to [~vern gitea](https://git.vern.cc/aryak/0x0-instances). Feel free to mirror the repo to your git instance too!

## How to add your instance?
Just submit a PR or email me a patch at aryak@vern.cc